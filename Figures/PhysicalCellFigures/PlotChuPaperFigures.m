%% For paper, need following plots:
% 4) Nyquist plots at different temperatures
% 6) UDDS
close all
clear all

% %% 2) 3d plot of R0
% figure(2); clf
% % load cell_est_2016_6_9_8_55_16.mat
% % load cell_est_2016_8_3_22_20_21.mat
% % load cell_est_25C_bestest_newformat.mat
% 
% R_data=cell_est.opt_vars.R_data;
% [Rall_data] = R0_data_gen_gui4(cell_est,1);
%   
% surf((Rall_data.Mag./cell_est.cst.Q),(1-Rall_data.Ahr0./cell_est.cst.Q)*100,Rall_data.R*1000);hold on;
%   %     EIS_info_temp_index=(cell_est.opt_vars.freq_data.EIS_info(:,8)==298.15);
%   %     scatter3(cell_est.opt_vars.freq_data.EIS_info(EIS_info_temp_index,4),...
%   %         1-cell_est.opt_vars.freq_data.EIS_info(EIS_info_temp_index,3)./cell_est.cst.Q,...
%   %         cell_est.opt_vars.freq_data.EIS_info(EIS_info_temp_index,1)*1000,'r'); hold on;
% 
% scatter3((R_data.Mag__est(:)./cell_est.cst.Q),(1-R_data.Ahr0_est(:)./cell_est.cst.Q)*100,R_data.R_est(:)*1000,'g');
% hold on;
% scatter3((R_data.Mag(:)./cell_est.cst.Q),(1-R_data.Ahr0(:)./cell_est.cst.Q)*100,R_data.R(:)*1000,'b');    
% hold off;
% hx=xlabel('C-rate');
% hy=ylabel('SOC (%)');
% hz=zlabel('Resistance (m\Omega)');
% ht=title('Resistance values versus SOC & C-rate');
% %   legend('Frequency R0 Data','Estimated Data','Measured Data');
% % campos([-70.9928 -118.4719    2.0741])
% % zlim([1 1.3]);
% % notesFormat3d([0.2 0.1 0.3 0])
% thesisFormat3d([0.2 0.1 0.3 0]);
% x_lim=get(gca,'xlim');
% x_lim_scale=x_lim(2)-x_lim(1);
% x_lim_mid=x_lim_scale/2+x_lim(1);
% y_lim=get(gca,'ylim');
% y_lim_scale=y_lim(2)-y_lim(1);
% y_lim_mid=y_lim_scale/2+y_lim(1);
% z_lim=get(gca,'zlim');
% z_lim_scale=z_lim(2)-z_lim(1);
% z_lim_mid=z_lim_scale/2+z_lim(1);
% set(hx,'position',[x_lim_mid-.1*x_lim_scale,y_lim(1)-.3*y_lim_scale,z_lim(1)]);
% set(hy,'position',[x_lim(1)-.3*x_lim_scale,y_lim_mid-.1*y_lim_scale,z_lim(1)]); 
% set(hz,'position',[x_lim(1),y_lim(2)+.4*y_lim_scale,z_lim_mid-.1*z_lim_scale]);
% set(ht,'position',[x_lim_mid+.35*x_lim_scale,y_lim(2),z_lim(2)+.05*z_lim_scale]);
% 
% %% 3) R0 1-d
% clear
% figure(3); clf;
% % load cell_est_2015_10_22_19_36_47.mat
% % load cell_est_2016_2_1_16_56_29.mat
% load cell_est_temp_dependent.mat
%   cell_est.cst.Eact_kappa=0;
%   cell_est.neg.Eact_sigma=0;
%   cell_est.pos.Eact_sigma=0;
%   cell_est.neg.Eact_Rdl=0;
%   cell_est.pos.Eact_Rdl=0;
%   
% R_data=cell_est.opt_vars.R_data;
% [Rall_data] = R0_data_gen_gui4(cell_est,1);
% 
% scatter((1-R_data.Ahr0_est(:)./cell_est.cst.Q)*100,R_data.R_est(:)*1000,'xr'); hold on;
% scatter((1-R_data.Ahr0(:)./cell_est.cst.Q)*100,R_data.R(:)*1000,'b'); hold off;
% legend('Estimated Data','Measured Data');
% 
% hx=xlabel('SOC (%)');
% hy=ylabel('Resistance (m\Omega)');
% ht=title('R_0 values versus SOC at all C-Rates');
% grid on;
% thesisFormat([0,0,0,.08]);

  
%% 4) Nyquist
close all; clear
load cell_est_25C_best_calidata.mat % [works]

freq_data=cell_est.opt_vars.freq_data;

figure(4); clf
% hf = gcf; set(hf,'position',get(hf,'position') .* [1 1 1.1 0.91]);
% num_temps=unique(freq_data.T);
% jj=find((freq_data.omega==min(freq_data.omega)));
% jj=[1;jj+1];
% freq_data.Z = freq_data.Z*1000;
% dotsize = 10;
% linewidth = 1.5;
% scale = [4.5 2.7 1.8 1.75 1.5];
% for pp=1:length(num_temps)
%   subplot(2,3,pp);
%   temp_index_start=(pp-1)*10+1;
%   temp_index_stop=(pp-1)*10+11;
%   ii=jj(temp_index_start:temp_index_stop);
% 
%   plot(real(freq_data.Z(ii(1):ii(2)-1,1)),-imag(freq_data.Z(ii(1):ii(2)-1,1)),'.b','markersize',dotsize);hold on
%   plot(real(freq_data.Z(ii(4):ii(5)-1,1)),-imag(freq_data.Z(ii(4):ii(5)-1,1)),'.k','markersize',dotsize);
%   plot(real(freq_data.Z(ii(10):ii(11)-1,1)),-imag(freq_data.Z(ii(10):ii(11)-1,1)),'.r','markersize',dotsize);
%   Z_temp=freq_data.Z_est*1000;
% 
%   plot(real(Z_temp(ii(1):ii(2)-1,1)),-imag(Z_temp(ii(1):ii(2)-1,1)),'b','linewidth',linewidth);hold on;
%   plot(real(Z_temp(ii(4):ii(5)-1,1)),-imag(Z_temp(ii(4):ii(5)-1,1)),'k','linewidth',linewidth);
%   plot(real(Z_temp(ii(10):ii(11)-1,1)),-imag(Z_temp(ii(10):ii(11)-1,1)),'r','linewidth',linewidth);
%   hx = xlabel('Real Z (m\Omega)'); hy = ylabel('-Imag Z (m\Omega)');
%   ht = title(sprintf('T=%d^oC',num_temps(pp)-273.15));hold off;
%   set(ht,'fontweight','normal')
%   set(ht,'fontname','Times');
%   set(hx,'fontname','Times');
%   set(hy,'fontname','Times');
%   axis([0 scale(pp) 0 scale(pp)]);
%   if pp == 1
%     hl = legend('100% SOC meas','50% SOC meas','10% SOC meas',...
%                 '100% SOC est','50% SOC est','10% SOC est');
%   end
%   grid on
% end

% hf = gcf; set(hf,'position',get(hf,'position') .* [1 1 1.1 0.91]);
num_temps=unique(freq_data.T);
jj=find((freq_data.omega==min(freq_data.omega)));
jj=[1;jj+1];
freq_data.Z = freq_data.Z*1000;
dotsize = 10;
linewidth = 1.5;
scale = [4.5 2.7 1.8 1.75 1.5];
pp = find(num_temps == 298.15);
% for pp=1:length(num_temps)
%   subplot(2,3,pp);
  temp_index_start=(pp-1)*10+1;
  temp_index_stop=(pp-1)*10+11;
  ii=jj(temp_index_start:temp_index_stop);

  plot(real(freq_data.Z(ii(1):ii(2)-1,1)),-imag(freq_data.Z(ii(1):ii(2)-1,1)),'.b','markersize',dotsize);hold on
  plot(real(freq_data.Z(ii(4):ii(5)-1,1)),-imag(freq_data.Z(ii(4):ii(5)-1,1)),'.k','markersize',dotsize);
  plot(real(freq_data.Z(ii(10):ii(11)-1,1)),-imag(freq_data.Z(ii(10):ii(11)-1,1)),'.r','markersize',dotsize);
  Z_temp=freq_data.Z_est*1000;

  plot(real(Z_temp(ii(1):ii(2)-1,1)),-imag(Z_temp(ii(1):ii(2)-1,1)),'b','linewidth',linewidth);hold on;
  plot(real(Z_temp(ii(4):ii(5)-1,1)),-imag(Z_temp(ii(4):ii(5)-1,1)),'k','linewidth',linewidth);
  plot(real(Z_temp(ii(10):ii(11)-1,1)),-imag(Z_temp(ii(10):ii(11)-1,1)),'r','linewidth',linewidth);
  hx = xlabel('Real Z (m\Omega)'); hy = ylabel('-Imag Z (m\Omega)');
  title('Nyquist plot for 25 Ah cell');
%   set(ht,'fontweight','normal')
%   set(ht,'fontname','Times');
%   set(hx,'fontname','Times');
%   set(hy,'fontname','Times');
  axis([0 scale(pp) 0 scale(pp)]);
%   if pp == 1
    hl = legend('100% SOC meas','50% SOC meas','10% SOC meas',...
                '100% SOC est','50% SOC est','10% SOC est','location','northwest');
%   end
  grid on
% end
axis square
thesisFormat
print -deps2c Nyquist.eps

% %% 5) Rss
% clear
% figure(5); clf;
% load cell_est_25C_best_calidata.mat % [works]
% % load cell_est_2016_2_1_16_56_29.mat % [works]
% 
% load cell_est_Ds_SOC_dependent2.mat
% 
% Rss_data=cell_est.opt_vars.Rss_data;
%   
% if isfield(Rss_data,'Z_low_high')
%   plot(Rss_data.SOC(Rss_data.Mag>0)*100,real(Rss_data.Z_low_high(Rss_data.Mag>0))*1000,'r');hold on
% else
%   plot(Rss_data.SOC(Rss_data.Mag>0)*100,real(Rss_data.Z(Rss_data.Mag>0))*1000,'r');hold on    
% end
% plot(Rss_data.SOC(Rss_data.Mag>0)*100,real(Rss_data.Z_est(Rss_data.Mag>0))*1000,'b');hold on
% %     plot(Rss_data.SOC(Rss_data.Mag<0),real(Rss_data.Z(Rss_data.Mag<0))*1000,'c');
% %     plot((1-Rss_data.Ahr0./cell_est.cst.Q)*100,real(Rss_data.Z_est)*1000,'r');hold on
% %     plot((1-Rss_data.Ahr0./cell_est.cst.Q)*100,real(Rss_data.Z)*1000,'.b');
% hold off;grid;
% xlabel('SOC (%)');
% ylabel('Z (m\Omega)');
% title('Steady-state discharge resistance values versus SOC');
% legend('Truth','Estimated');
% thesisFormat  
% 

%% 6) UDDS
load cell_est_2016_8_3_22_20_21.mat
% load cell_est_25C_best_calidata.mat % [works]

t0 = 6000;

if isfield(cell_est.opt_vars,'dyn_data')
  if isfield(cell_est.opt_vars.dyn_data,'udds')
    time_raw=cell_est.opt_vars.dyn_data.udds.time_raw;
    V_raw=cell_est.opt_vars.dyn_data.udds.V_raw;
    ind = find(time_raw>t0,1,'first');
    time_raw = time_raw(ind:end)-t0;
    V_raw = V_raw(ind:end);
    time_ROM=cell_est.opt_vars.dyn_data.udds.time_ROM;
    V_ROM=cell_est.opt_vars.dyn_data.udds.V_ROM;
    ind = find(time_ROM > t0,1,'first');
    time_ROM = time_ROM(ind:end)-t0;
    V_ROM = V_ROM(ind:end);

    figure(6); clf;
    plot(time_ROM(1:end-1)/3600,V_ROM(2:end));
    hold on
    plot(time_raw(1:end-1)/3600,V_raw(1:end-1),'--');
    hold off; xlim([0 10.25])
    xlabel('Time (h)');
    ylabel('Voltage (V)');
    title('UDDS cell voltage');
    legend('Estimated','Truth');
    grid on
    thesisFormat([0.1 0 0 0])   
    print -deps2c UDDSmatch.eps
    
    figure(16); clf;
    plot(time_ROM(1:end-1)/3600,V_ROM(2:end));
    hold on
    plot(time_raw(1:end-1)/3600,V_raw(1:end-1),'--');
    hold off; xlim([3.65 4.15]); ylim([3.58 3.85])
    xlabel('Time (h)');
    ylabel('Voltage (V)');
    title('UDDS cell voltage (zoom)');
    legend('Estimated','Truth');
    grid on
    thesisFormat([0.1 0 0 0])
    print -deps2c UDDSmatchzoom.eps
  end
end

    v_interp = interp1(time_raw,V_raw,time_ROM(1:end-1));
    v_err = v_interp - V_ROM(2:end);
    rms(v_err(~isnan(v_err)))

    figure(7); clf;
    plot(time_ROM(1:end-1)/3600,1000*v_err);
    xlabel('Time (h)'); xlim([0 10.25])
    ylabel('Voltage (mV)');
    title('UDDS cell voltage-prediction error');
    grid on
    thesisFormat 
    print -deps2c UDDSerr.eps

    figure(17); clf;
    plot(time_ROM(1:end-1)/3600,1000*v_err);
    xlabel('Time (h)'); xlim([3.65 4.15])
    ylabel('Voltage (mV)');
    title('UDDS cell voltage-prediction error (zoom)');
    grid on
    thesisFormat 
    print -deps2c UDDSerrzoom.eps
