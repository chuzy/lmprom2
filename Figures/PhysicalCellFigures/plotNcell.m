% Final theta0n = 1.00085, theta100n = 0.225661
% Final theta0p = 0.221008, theta100p = 0.916653

green = [0 0.75 0];
BLUE4 = [0 0 128]/256;
GREEN4 = [6 131 0]/256;
MAGENTA4 = [125 0 127]/256;
RED4 = [125 0 0]/256;
RED = [1 0 0];

load('OCV_testing/Lower_Negmodel-ocv.mat'); neg1 = model;
load('OCV_testing/Lower_Posmodel-ocv.mat'); pos1 = model;
load('OCV_testing/Upper_Negmodel-ocv.mat'); neg2 = model;
load('OCV_testing/Upper_Posmodel-ocv.mat'); pos2 = model;
load('OCV_testing/FORD_55model-ocv.mat'); ford1 = model;
load('OCV_testing/FORD_56model-ocv.mat'); ford2 = model;
  
negmodel = neg1; negmodel.OCV0 = (neg1.OCV0+neg2.OCV0)/2;
                 negmodel.OCVrel = (neg1.OCVrel+neg2.OCVrel)/2;
posmodel = pos1; posmodel.OCV0 = (pos1.OCV0+pos2.OCV0)/2;
               posmodel.OCVrel = (pos1.OCVrel+pos2.OCVrel)/2;
cellmodel = ford1; cellmodel.OCV0 = (ford1.OCV0+ford2.OCV0)/2;
                   cellmodel.OCVrel = (ford1.OCVrel+ford2.OCVrel)/2;

plot1 = 0;                   
plot2 = 0;
plot3 = 1;

if plot3
  msizex = 10;
  msizeo = 8;
  figure(1); clf; h = plot(rand(1,10)); lightblue = get(h,'color'); clf;

  subplot(2,2,3)
  zn = 0:0.01:1.01; znmax = max(zn);
  theta0n = 1.00085;  theta100n = 0.225661;
  plot(znmax-theta0n,OCVfromSOCtemp(theta0n,25,negmodel),'rx','markersize',msizex); hold on
  plot(znmax-theta100n,OCVfromSOCtemp(theta100n,25,negmodel),'o','color',GREEN4,'markersize',msizeo); 
  plot(znmax-zn,OCVfromSOCtemp(zn,25,negmodel),'b-'); hn = gca;
  xlim([0 znmax]);
  title('Negative-electrode coin-cell OCP');
  xlabel('Degree of lithiation NEG');
  set(gca,'xtick',(0:0.25:1)*znmax);
  set(gca,'xticklabel',{'0','0.25','0.5','0.75','1'});
  ylabel('Open-circuit potential (V)');
  grid on

  subplot(2,2,1)
  zc = 0:0.01:1; zcmax = max(zc);
  plot(0,OCVfromSOCtemp(0,25,cellmodel),'rx','markersize',msizex); hold on
  plot(1,OCVfromSOCtemp(1,25,cellmodel),'o','color',GREEN4,'markersize',msizeo);
  plot(zc,OCVfromSOCtemp(zc,25,cellmodel),'b-'); hc = gca;
  title('Full-cell OCV relationship');
  xlabel('Cell state of charge');
  set(gca,'xtick',(0:0.25:1)*zcmax);
  set(gca,'xticklabel',{'0','0.25','0.5','0.75','1'});
  ylabel('Open-circuit voltage (V)');
  ylim([2.3 4.15])
  grid on

  subplot(2,2,2)
  zp = 0:0.01:1; zpmax = max(zp);
  theta0p = 0.221008; theta100p = 0.916653;
  plot(zpmax-theta0p,OCVfromSOCtemp(theta0p,25,posmodel),'rx','markersize',msizex); hold on
  plot(zpmax-theta100p,OCVfromSOCtemp(theta100p,25,posmodel),'o','color',GREEN4,'markersize',msizeo);
  plot(zpmax-zp,OCVfromSOCtemp(zp,25,posmodel),'b-'); hp = gca;
  xlim([0 zpmax]); ylim([3.3 4.3])
  title('Positive-electrode coin-cell OCP');
  xlabel('Degree of lithiation POS');
  set(gca,'xtick',(0:0.25:1)*zpmax);
  set(gca,'xticklabel',{'0','0.25','0.5','0.75','1'});
  ylabel('Open-circuit potential (V)');
  grid on
  
  subplot(2,2,4);
  z = 0:0.01:1;
  OCV = OCVfromSOCtemp(z,25,cellmodel);
  zn = theta0n + z*(theta100n-theta0n);
  OCPn = OCVfromSOCtemp(zn,25,negmodel);
  zp = theta0p + z*(theta100p-theta0p);
  OCPp = OCVfromSOCtemp(zp,25,posmodel);
  plot(z,OCV,'r',z,OCPp-OCPn,'b');
  title('Measured and estimated OCV relationships');
  xlabel('Cell state of charge');
  set(gca,'xtick',(0:0.25:1)*zcmax);
  set(gca,'xticklabel',{'0','0.25','0.5','0.75','1'});
  ylabel('Open-circuit voltage (V)');
  legend('Measured OCV','Estimated OCV','location','southeast');
  ylim([2.3 4.15])
  grid on
  rms(OCV - (OCPp-OCPn))

%   subplot(2,2,4)
%   axis([0 1 0 1]);
%   text(0,1,'TAG');
%   set(gca,'visible','off')
  
%   notesFormat([0.05 0.03 0 0]);
  thesisFormat

  [xn0,yn0]     = ds2nfu(hn,znmax-theta0n,OCVfromSOCtemp(theta0n,25,negmodel));
  [xn100,yn100] = ds2nfu(hn,znmax-theta100n,OCVfromSOCtemp(theta100n,25,negmodel));
  [xc0,yc0]     = ds2nfu(hc,0,OCVfromSOCtemp(0,25,cellmodel));
  [xc100,yc100] = ds2nfu(hc,zcmax,OCVfromSOCtemp(1,25,cellmodel));
  [xp0,yp0]     = ds2nfu(hp,zpmax-theta0p,OCVfromSOCtemp(theta0p,25,posmodel));
  [xp100,yp100] = ds2nfu(hp,zpmax-theta100p,OCVfromSOCtemp(theta100p,25,posmodel));

  % annotation('doublearrow',[xf xf2],[yf yf2],'color','g')
  annotation('line',[xn0 xc0],[yn0 yc0],'color',RED,'linestyle','--','linewidth',2);
  annotation('line',[xc0 xp0],[yc0 yp0],'color',RED,'linestyle','--','linewidth',2);
  annotation('line',[xn100 xc100],[yn100 yc100],'color',GREEN4,'linestyle','--','linewidth',2);
  annotation('line',[xc100 xp100],[yc100 yp100],'color',GREEN4,'linestyle','--','linewidth',2);

  h = legend(hn,'Q0N','Q1N','location','northwest'); set(h,'fontsize',30);
  h = legend(hp,'Q0P','Q1P','location','northeast'); set(h,'fontsize',30);
  h = legend(hc,'Z0','Z100','location','northwest'); set(h,'fontsize',30);

  print -deps2c ../OCVs.eps
%   !open OCVs.eps
end

if plot2
  figure(2); clf
  zn = 0:0.01:1.01; znmax = max(zn);
  theta0n = 1.00085;  theta100n = 0.225661;
%   plot(znmax-theta0n,OCVfromSOCtemp(theta0n,25,negmodel),'rx'); hold on
%   plot(znmax-theta100n,OCVfromSOCtemp(theta100n,25,negmodel),'o','color',green); 
  plot(znmax-zn,OCVfromSOCtemp(zn,25,negmodel)); %hn = gca;
  xlim([0 znmax]);
  title('Negative-electrode coin-cell OCP');
  xlabel('Degree of lithiation NEG');
  set(gca,'xtick',(0:0.25:1)*znmax);
  set(gca,'xticklabel',{'0','0.25','0.5','0.75','1'});
  ylabel('Open-circuit potential (V)');
  grid on
  notesFormat([0.05 0 0 0])
  print -deps2c negOCP.eps
  
  figure(3); clf
  zp = 0:0.01:1; zpmax = max(zp);
  theta0p = 0.221008; theta100p = 0.916653;
%   plot(zpmax-theta0p,OCVfromSOCtemp(theta0p,25,posmodel),'rx'); hold on
%   plot(zpmax-theta100p,OCVfromSOCtemp(theta100p,25,posmodel),'o','color',green);
  plot(zpmax-zp,OCVfromSOCtemp(zp,25,posmodel));%hp = gca;
  xlim([0 zpmax]); ylim([3.3 4.3])
  title('Positive-electrode coin-cell OCP');
  xlabel('Degree of lithiation POS');
  set(gca,'xtick',(0:0.25:1)*zpmax);
  set(gca,'xticklabel',{'0','0.25','0.5','0.75','1'});
  ylabel('Open-circuit potential (V)');
  grid on
  notesFormat([0.05 0 0 0])
  print -deps2c posOCP.eps
  
end

if plot1
    figure(3); clf;
  zc = 0:0.01:1; zcmax = max(zc);
%   plot(0,OCVfromSOCtemp(0,25,cellmodel),'rx'); hold on
%   plot(1,OCVfromSOCtemp(1,25,cellmodel),'o','color',green); 
  plot(zc,OCVfromSOCtemp(zc,25,cellmodel)); %hc = gca;
  title('Full-cell OCV relationship');
  xlabel('State of charge');
  set(gca,'xtick',(0:0.25:1)*zcmax);
  set(gca,'xticklabel',{'0','0.25','0.5','0.75','1'});
  ylabel('Open-circuit voltage (V)');
  ylim([2.3 4.15])
  grid on
  notesFormat([0.05 0 0 0])
  print -deps2c cellOCV.eps
  

end